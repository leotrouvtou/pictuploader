class AddPhotoFileNameToPict < ActiveRecord::Migration
  def change
    add_column :picts, :photo_file_name, :string
  end
end
