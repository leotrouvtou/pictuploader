class AddPhotoContentTypeToPict < ActiveRecord::Migration
  def change
    add_column :picts, :photo_content_type, :string
  end
end
