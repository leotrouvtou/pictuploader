class AddPhotoFileSizeToPict < ActiveRecord::Migration
  def change
    add_column :picts, :photo_file_size, :int
  end
end
