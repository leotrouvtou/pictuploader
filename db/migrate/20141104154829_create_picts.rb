class CreatePicts < ActiveRecord::Migration
  def change
    create_table :picts do |t|
      t.string :photo

      t.timestamps
    end
  end
end
