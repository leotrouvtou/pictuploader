json.array!(@picts) do |pict|
  json.extract! pict, :id, :photo
  json.url pict_url(pict, format: :json)
end
