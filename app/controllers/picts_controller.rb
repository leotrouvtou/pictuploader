class PictsController < ApplicationController
  before_action :set_pict, only: [:show, :edit, :update, :destroy]

  # GET /picts
  # GET /picts.json
  def index
    @picts = Pict.paginate(:page => params[:page], :per_page => 18).order('created_at DESC')

  end

  # GET /picts/1
  # GET /picts/1.json
  def show
  end

  # GET /picts/new
  def new
    @pict = Pict.new
  end

  # GET /picts/1/edit
  def edit
  end

  # POST /picts
  # POST /picts.json
  def create
    @pict = Pict.new(pict_params)

    respond_to do |format|
      if @pict.save
        format.html { redirect_to @pict, notice: 'Pict was successfully created.' }
        format.json { render :show, status: :created, location: @pict }
      else
        format.html { render :new }
        format.json { render json: @pict.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /picts/1
  # PATCH/PUT /picts/1.json
  def update
    respond_to do |format|
      if @pict.update(pict_params)
        format.html { redirect_to @pict, notice: 'Pict was successfully updated.' }
        format.json { render :show, status: :ok, location: @pict }
      else
        format.html { render :edit }
        format.json { render json: @pict.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /picts/1
  # DELETE /picts/1.json
  def destroy
    @pict.destroy
    respond_to do |format|
      format.html { redirect_to picts_url, notice: 'Pict was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pict
      @pict = Pict.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def pict_params
      params.require(:pict).permit(:photo)
    end
end
