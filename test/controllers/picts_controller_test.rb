require 'test_helper'

class PictsControllerTest < ActionController::TestCase
  setup do
    @pict = picts(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:picts)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create pict" do
    assert_difference('Pict.count') do
      post :create, pict: { photo: @pict.photo }
    end

    assert_redirected_to pict_path(assigns(:pict))
  end

  test "should show pict" do
    get :show, id: @pict
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @pict
    assert_response :success
  end

  test "should update pict" do
    patch :update, id: @pict, pict: { photo: @pict.photo }
    assert_redirected_to pict_path(assigns(:pict))
  end

  test "should destroy pict" do
    assert_difference('Pict.count', -1) do
      delete :destroy, id: @pict
    end

    assert_redirected_to picts_path
  end
end
